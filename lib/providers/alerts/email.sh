#!/usr/bin/env bash

source config/providers/alerts/email.sh

alert_message="${1}"

if [[ ${email} -eq 1 ]]; then
    if [[ ${#email_list[@]} -gt 0 ]]; then
        echo "${alert_message}" | mailx -s "[ALERT] ${tool_name} on ${hostname_short} returned an error!" "${email_list[@]}"
    else
        logger "No alert was sent, email_list appears to be empty. Please set it in ${base_dir}/config/providers/alerts/email.sh ..."
    fi
fi
