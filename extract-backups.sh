#!/usr/bin/env bash
shopt -s nullglob

# Set tool_name for logging/alerts
tool_name='EXTRACT_BACKUPS'

# Set base_dir and go to it.
base_dir=$( dirname "$( readlink -f $0 )" )
cd "${base_dir}"

## Import required config / libraries
## Local includes
source config/base_config.sh

# Util library includes.
source lib/logger.sh

backup_to_restore="${backupdir}/${1}"

dbdist=$( mysql --version | grep MariaDB )

if [[ -n "${dbdist}" ]]; then
    logger "Selected mariabackup as the backupbin ..."
    backupbin=mariabackup
    streambin=mbstream
else
    logger "Selected xtrabackup as the backupbin ..."
    backupbin=xtrabackup
    streambin=xbstream
fi

if [[ -z $( command -v qpress ) ]]; then
    logger "qpress not found. Please install the qpress package."
    exit 1
fi

# Should be made more robust, actually look in the supplied dir for a full-*xbstream file.
if [[ "${1}" = "" ]] || [[ ! -d "${backupdir}/${1}" ]]; then
    echo "Directory not specified or does not exist."
    echo ""
    echo "Usage: extract-backups.sh [backup_(date_stamp)]"
    echo ""
    echo "Available backup directories: "
    bdirs=( $( ls "${backupdir}" ) )
    for x in "${bdirs[@]}"
    do
        if [[ "${x}" != "restore" ]]; then
            echo -e "    ${x}"
        fi
    done
    echo ""
    exit 1
fi

logger "Starting extract script ..."

cd "${backup_to_restore}"

backup_opts=( "--decompress" )

if [[ ${parallel} -eq 1 ]]; then
    logger "Parallel compression is enabled in the config ..."
    # Make sure we have nproc, even though there are other ways.
    if [[ -n $( command -v nproc ) ]]; then
        nproc=$( echo "$( nproc --all ) / 2" | bc | awk '{print int($1+0.5)}' )
        logger "Setting --parallel to ${nproc} ( Total cores / 2, rounded up )"
        backup_opts+=(
            "--parallel=${nproc}"
        )
    else
        logger "nproc command not found, parallel decompression will be disabled ..."
    fi
fi

files=( "${backup_to_restore}"/*.xbstream )

for file in "${files[@]}"
do
    base_filename="$( basename "${file%.xbstream}" )"
    restore_dir="${backupdir}/restore/${base_filename}"
    mkdir -p "${restore_dir}"
    logger "Extracting ${file} ..."
    "${streambin}" -x -C "${restore_dir}" < "${file}"

    # "Bug" in MariaBackup, caused by --extra-lsndir
    if [[ -f "${backup_to_restore}/xtrabackup_info.qp" ]]; then
        rm -f "${backup_to_restore}/xtrabackup_info.qp"
    fi

    logger "Decompressing contents of ${base_filename} ..."

    "${backupbin}" "${backup_opts[@]}" --target-dir="${restore_dir}" 2> "${log_dir}/extract-${base_filename}-${date_stamp}.log"

    find "${restore_dir}" -name '*.qp' -delete

    res=$( grep 'completed OK' "${log_dir}/extract-${base_filename}-${date_stamp}.log" )
    if [[ -n "${res}" ]]; then
        continue
    else
        logger "Looks like there was an error extracting ${base_filename}."
        echo ""
        echo "Please review ${log_dir}/extract-${base_filename}-${date_stamp}.log for any errors."
        break
    fi
done

logger "Extraction complete."
echo ""
echo "You should now run the prepare-backup.sh script if there were no errors"
