# This file must be loaded AFTER the config file for the script you are running.
# Without it, TOOL_NAME isn't set.

# Set your local DB User and Password
db_user='root'
db_pass=''


##
## Alerting Config
##

# Enable/Disable alerting
alerting=0

# Stop alert spam if a check/scripts runs often. It's best to leave this on.
# ALERT_SLEEP_MINUTES controls how long it "sleeps" for.
stop_alert_spam=1

# How long to sleep between alerts in minutes
alert_sleep_minutes=30


##
## Backup / extract / prepare config
##

# Full path to your backup directory.
backupdir=''

# Full path where you want the backup rebuild done
# By default: ${backupdir}/restore
restoredir="${backupdir}/restore"

# Enable or disable incremental backups
incremental=0

# Use Compression - On by default, only used if incrementals are enabled.
compression=1

# Use parallel compression, only used if incrementals are enabled.
parallel=1

# Number of days of backups to keep
keep_days=3


##
## Max Connection Watcher Config
##

# Percent expressed as decimal
max_conn_allowed_pct="0.80"

##
## Heartbeat watcher config (replication lag)
warning_lag_seconds=300
critical_lag_seconds=900

##
## Logging config
##

# Echo to console
verbose=1

# Set our logging directory
# Don't change this unless you know what's going on!
log_dir="${base_dir}/logs/${tool_name}"
log_file="${log_dir}/${date_stamp}.log"

##
# Things-you-shouldn't-touch Config
##

date_stamp=$( date "+%F" )
timestamp=$( date +%s )

hostname_short=$( hostname -s )
hostname_full=$( hostname -a )
