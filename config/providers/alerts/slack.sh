## Enables the slack alert provider
## Set to 0 to turn it off
slack=0

## Webhook URL
slack_webhook_url=""

## Channel name or channel ID
slack_channel="general"

## Chat username
slack_username=""
