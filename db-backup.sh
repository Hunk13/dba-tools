#!/usr/bin/env bash
shopt -s nullglob

# Set tool_name for logging/alerts
tool_name='BACKUPS'

# Set base_dir and go to it.
base_dir=$( dirname "$( readlink -f $0 )" )
cd "${base_dir}"


## Import required config / libraries
## Local includes
source config/base_config.sh

# Util library includes.
source lib/logger.sh

logger "Starting backup script ..."

# Some sanity checks
if [[ -z "${db_user}" ]]; then
    logger "db_user is not set. Check ${base_dir}/config/base_config.sh"
    exit 1
fi

if [[ -z "${backupdir}" ]] ; then
    logger "Backup directory isn't set. Check ${base_dir}/config/base_config.sh"
    exit 1
fi

if [[ -z "${keep_days}" ]]; then
    keep_days=3
fi

db_opts=( "-u${db_user}" )

if [[ -n "${db_pass}" ]]; then
    db_opts+=( "-p${db_pass}" )
fi

logger "Basic checks passed ..."

backupdir_today="${backupdir}/backup_${date_stamp}"

# Figure out if we're going to use mariabackup or xtrabackup.. if not already set.
#
# In general, if you are running MariaDB, you should be using an equal or higher version of Mariabackup.
# We simply assume the admin knows what should be installed.

# Find our backupbin based on installed distribution.
dbdist=$( mysql --version | grep MariaDB )
if [[ -n "${dbdist}" ]]; then
    logger "Selected mariabackup as the backupbin ..."
    backupbin=mariabackup
else
    logger "Selected xtrabackup as the backupbin ..."
    backupbin=xtrabackup
fi

# And now to fire off the backup process.
if [[ ${incremental} -eq 1 ]]; then

    logger "Incremental backup process starting ..."

    backup_opts=(
        "--backup"
        "--target-dir=${backupdir_today}"
        "--stream=xbstream"
        "--extra-lsndir=${backupdir_today}"
    )

    # Determine if compression is on
    if [[ ${compression} -eq 1 ]]; then
        logger "Compression is enabled ..."
        backup_opts+=( "--compress" )

        #Let the user know if qpress is found or not
        if [[ ! -n $( command -v qpress ) ]]; then
            logger "NOTE: You do not appear have qpress installed. You will not be able to decompress the incremental backups."
        fi
    fi

    # Determine if parallel is on
    if [[ ${parallel} -eq 1 ]]; then
        logger "Parallel compression is enabled in the config ..."

        # Make sure we have nproc, even though there are other ways.
        if [[ -n $( command -v nproc ) ]]; then
            nproc=$( echo "$( nproc --all ) / 2" | bc | awk '{print int($1+0.5)}' )
            logger "Setting --parallel and --compress-threads to ${nproc} ( Total cores / 2, rounded up )"
            backup_opts+=(
                "--parallel=${nproc}"
                "--compress-threads=${nproc}"
            )
        else
            logger "nproc command not found, parallel compression will be disabled ..."
        fi
    fi

    backup_type='full'

    # Is there a checkpoints file? Incremental time.
    if [[ -f "${backupdir_today}/xtrabackup_checkpoints" ]]; then
        backup_type='incremental'
        lsn=$( awk '/to_lsn/ {print $3;}' "${backupdir_today}/xtrabackup_checkpoints" )
        backup_opts+=( "--incremental-lsn=${LSN}" )
    fi

    logger "Backup type: ${backup_type^} ..."

    # Run the backup process
    mkdir -p "${backupdir_today}"
    find "${backupdir_today}" -type f -name '*.incomplete' -delete
    "${backupbin}" "${db_opts[@]}" "${backup_opts[@]}" > "${backupdir_today}/${backup_type}-${date_stamp}-${timestamp}.xbstream.incomplete" 2> "${log_dir}/backup_${date_stamp}.log"
    mv "${backupdir_today}/${backup_type}-${date_stamp}-${timestamp}.xbstream.incomplete" "${backupdir_today}/${backup_type}-${date_stamp}-${timestamp}.xbstream"

    logger "Backup process completed ..."

else

    # Check if a full backup already exists
    if [[ -d "${backupdir_today}" ]]; then
        logger "Incremental backups are disabled and a directory for today exists. Backup process canceled."
        logger "If you intend to make a new backup, please remove or rename ${backupdir_today}."
        exit 1
    fi

    # Run a simple, full backup.
    "${backupbin}" "${db_opts[@]}" --backup --target-dir="${backupdir_today}" 2> "${log_dir}/backup_${date_stamp}.log"

    logger "Backup process completed ..."

fi

# Were we successful?
res=$( grep 'completed OK' "${log_dir}/backup_${date_stamp}.log" )
if [[ -z "${res}" ]]; then
    # This is not POSIX compliant. Total BASHism, which is just fine.
    source lib/alert.sh "Backup failed."
else
    logger "Backup successful ..."
    logger "Cleaning old backups ..."

    backups=( "${backupdir}"/backup_*/ )
    backup_cnt=${#backups[@]}
    if (( ${backup_cnt} > ${keep_days} )); then
        rm -r "${backups[@]:0:(( ${backup_cnt} - ${keep_days} ))}"
    fi
fi

# FIN
logger "Script completed."
exit 0
