#!/usr/bin/env bash

# Set tool_name for logging/alerts
tool_name='REPLICATION_STATE_MONITOR'

# Set base_dir and go to it.
base_dir=$( dirname "$( readlink -f $0 )" )
cd "${base_dir}"

# This script is used to monitor replication thread/sql state. Not lag.

## Import required info / config / libraries
## Local includes
source config/base_config.sh

# Util library includes.
source lib/logger.sh

db_opts=( "-u${db_user}" )

if [[ -n "${db_pass}" ]]; then
    db_opts+=( "-p${db_pass}" )
fi

dbdist=$( mysql --version | grep MariaDB )

if [[ -n "${dbdist}" ]]; then
    dist="mariadb"
    search_name="Connection_name"
else
    dist="mysql"
    search_name="Channel_Name"
fi

logger "Beginning replication state check ..."

slaves=( $( mysql "${db_opts[@]}" -e "${STATUS}\G" 2>&1 | grep -v "Using a password" | grep "${search_name}" | awk '{print $2}' ) )

# Not multi-source
if (( ${#slaves[@]} = 1 )); then
    logger "Single replication instance found ..."
    slave_io=$( mysql "${db_opts[@]}" -e "SHOW SLAVE STATUS\G" 2>&1 | grep -v "Using a password" | grep Slave_IO_Running | awk '{print $2}' )
    slave_sql=$( mysql "${db_opts[@]}" -e "SHOW SLAVE STATUS\G" 2>&1 | grep -v "Using a password" | grep Slave_SQL_Running | grep -v State | awk '{print $2}' )

    if [[ "${slave_io}" != "Yes" && "${slave_sql}" != "Yes" ]]; then
        logger "Replication is broken!"
        exit 0
    fi
else
    # Multisource
    for x in "${slaves[@]}"
    do
        logger "Checking replication on channel '${x}' ..."
    
        case "${dist}" in
            "mariadb" )
                slave_io=$( mysql "${db_opts[@]}" -e "SHOW SLAVE '${x}' STATUS\G" 2>&1 | grep -v "Using a password" | grep Slave_IO_Running | awk '{print $2}' )
                slave_sql=$( mysql "${db_opts[@]}" -e "SHOW SLAVE '${x}' STATUS\G" 2>&1 | grep -v "Using a password" | grep Slave_SQL_Running | grep -v State | awk '{print $2}' )
            ;;
            "mysql" )
                slave_io=$( mysql "${db_opts[@]}" -e "SHOW SLAVE STATUS FOR CHANNEL '${x}'\G" 2>&1 | grep -v "Using a password" | grep Slave_IO_Running | awk '{print $2}' )
                slave_sql=$( mysql "${db_opts[@]}" -e "SHOW SLAVE STATUS FOR CHANNEL '${x}'\G" 2>&1 | grep -v "Using a password" | grep Slave_SQL_Running | grep -v State | awk '{print $2}' )
            ;;
            * )
                echo "dist not set?"
                exit 1
            ;;
        esac

        if [[ "${slave_io}" != "Yes" && ${slave_sql} != "Yes" ]]; then
            logger "Replication is broken on ${x}!"
            exit 0
        else
            logger "Replication check passed on '${x}' ..."
        fi

    done
fi
