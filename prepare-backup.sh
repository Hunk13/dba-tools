#!/usr/bin/env bash
shopt -s nullglob

# Set tool_name for logging/alerts
tool_name='PREPARE_BACKUP'

# Set base_dir and go to it.
base_dir=$( dirname "$( readlink -f $0 )" )
cd "${base_dir}"

## Import required config / libraries
## Local includes
source config/base_config.sh

# Util library includes.
source lib/logger.sh

dbdist=$( mysql --version | grep MariaDB )
if [[ -n "${dbdist}" ]]; then
    logger "Detected MariaDB ..."
    backupbin=mariabackup
    restore_opts=( "--prepare" "--apply-log-only" )
else
    logger "Detected MySQL or MariaDB < 10.x ..."
    backupbin=xtrabackup
    restore_opts=( "--prepare" "--apply-log-only" )
fi

# Get the full + incremental dirs

full_dirs=( "${restoredir}"/full-*/ )
incr_dirs=( "${restoredir}"/incremental-*/ )

# Set our full backup
full_backup_dir="${full_dirs[0]}"

# Do we even have a full backup to prepare?
# Or maybe too many.
if [[ ${#full_dirs[@]} != 1 ]]; then
    logger "Full backup directory not found. Did you run extract-backups.sh?" 
    exit 1
fi

logger "Initial prepare of full backup ..."

"${backupbin}" "${restore_opts[@]}" --target-dir="${full_backup_dir}" 2> "${log_dir}/prepare-progress.log"

# Loop through incremental directories, if any, apply them to the full backup
for incr in "${incr_dirs[@]}"
do
    logger "Applying ${incr} to ${full_backup_dir} ..."
    "${backupbin}" "${restore_opts[@]}" --incremental-dir="${incr}" --target-dir="${full_backup_dir}" 2>> "${log_dir}/prepare-progress.log"
done

logger "Doing final prepare to full backup."

"${backupbin}" "${restore_opts[@]}" --target-dir="${full_backup_dir}" 2>> "${log_dir}/prepare-progress.log"

# "completed OK" appears as (Full + incrCount) + 1
ok_count="$(grep -c 'completed OK' "${log_dir}/prepare-progress.log")"

# Mariabackup, for one reason or another, generates 1 additional "completed OK!"
# We add one to systems that use xtrabackup in order to pass the check below.
if [[ "${backupbin}" = "xtrabackup" ]]; then
    ok_count=$((ok_count + 1))
fi

# Check our "completed OK!" count. Should be: #Full + #incr + 1
if (( ${ok_count} == (${#full_dirs[@]} + ${#incr_dirs[@]} + 1) )); then
    # Could use a heredoc, but I'm not a fan.
    echo ""
    logger "The backup appears to be fully prepared."
    echo ""
    echo "Please check the ${log_dir}/prepare-progress.log file to verify before continuing."
    echo ""
    echo "You will find the ready-to-restore backup in ${full_dirs[0]}"
    echo ""
    echo "To restore this, you will need to do the following:"
    echo "  * Stop MySQL"
    echo "  * Clean out your datadir ( Default: /var/lib/mysql )"
    echo "  * ${backupbin} --copy-back --target-dir=${full_dirs[0]}"
    echo ""
    echo "After that, ensure the mysql user owns all the files in the datadir and start mysql."
    echo ""
else
    echo ""
    echo "Prepare failed. Check ${log_dir}/prepare-progress.log"
fi
